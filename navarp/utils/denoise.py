import os
from os.path import dirname
import numpy as np
import torch
from navarp.extras import srcnn



def denoise_slice(data):
    """
    Apply a deep neural network to denoise a volume of data.

    Args:
        data (ndarray): A 2D numpy array representing a slice of data to be denoised.

    Returns:
        ndarray: A 2D numpy array representing the denoised data of the slice
    """
    # Load the model
    model = load_denoise_model()

    # Move the model to the appropriate device
    device = get_device()
    model.to(device)

    # Prepare the data
    noisy = torch.tensor(data).to(device)

    # Apply the model to the data
    with torch.no_grad():
        out = model(noisy.unsqueeze(1).unsqueeze(1))
        denoised_spectra = out.squeeze(1).detach().cpu().numpy()

    return denoised_spectra[0, :, 0, :]


def load_denoise_model():
    """
    Load the pretrained denoising model.

    Returns:
        SrCNN: A PyTorch model for denoising
    """
    path_navarp = dirname(dirname(__file__))
    path_extras = os.path.join(path_navarp, 'extras')
    path_navarp_denoise_model = os.path.join(path_extras, 'weights.pt')
    model = srcnn.SrCNN()
    model.load_state_dict(torch.load(path_navarp_denoise_model))
    return model


def get_device():
    """
    Get the appropriate PyTorch device to run the model on.

    Returns:
        device: A PyTorch device object (cpu or cuda)
    """
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    return device
