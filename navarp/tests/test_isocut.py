import numpy as np
import pytest
from navarp.utils.isocut import lines_sum, maps_sum


@pytest.fixture
def test_input():
    np.random.seed(42)
    value = np.random.randint(0, 128)
    delta = np.random.uniform(0, 10)
    array = np.linspace(0, 128, 11, dtype=np.int64)
    matrix = np.random.rand(128, 128, 128)
    axis = 2
    return value, delta, array, matrix, axis

def test_maps_sum_output_shape(test_input):
    value, delta, array, matrix, axis = test_input
    output = maps_sum(value, delta, array, matrix, axis=axis)
    expected_shape = (matrix.shape[0], matrix.shape[1])
    assert output.shape == expected_shape

def test_maps_sum_output_counts(test_input):
    value, delta, array, matrix, axis = test_input
    output, num_ind = maps_sum(value, delta, array, matrix, axis=axis, return_counts=True)
    expected_count = 1
    assert num_ind == expected_count


@pytest.fixture
def test_2d_input():
    np.random.seed(42)
    value = np.random.randint(0, 128)
    delta = np.random.uniform(0, 10)
    array = np.linspace(0, 128, 11, dtype=np.int64)
    matrix = np.random.rand(128, 128)
    axis = 1
    return value, delta, array, matrix, axis

def test_lines_sum_output_shape(test_2d_input):
    value, delta, array, matrix, axis = test_2d_input
    output = lines_sum(value, delta, array, matrix, axis=axis)
    expected_shape = (matrix.shape[1],)
    assert output.shape == expected_shape

def test_lines_sum_output_counts(test_2d_input):
    value, delta, array, matrix, axis = test_2d_input
    output, num_ind = lines_sum(value, delta, array, matrix, axis=axis, return_counts=True)
    expected_count = 1
    assert num_ind == expected_count