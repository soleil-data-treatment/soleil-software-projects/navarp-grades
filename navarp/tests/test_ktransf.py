import pytest
import numpy as np
from navarp.utils.ktransf import get_p_hv_along_slit, get_p_hv_perp_slit

@pytest.fixture
def test_input():
    hv = np.array([100, 200, 300])
    tht_ph = np.array([10, 20, 30, 40])
    return hv, tht_ph

def test_get_p_hv_along_slit_output_shape(test_input):
    hv, tht_ph = test_input
    output = get_p_hv_along_slit(hv, tht_ph)
    expected_shape = (len(hv), len(tht_ph))
    assert output.shape == expected_shape

def test_get_p_hv_along_slit_output_values(test_input):
    hv, tht_ph = test_input
    output = get_p_hv_along_slit(hv, tht_ph)
    expected_output = np.outer(hv, np.sin(np.radians(tht_ph))) * 2 * np.pi / 12400
    assert np.allclose(output, expected_output)


@pytest.fixture
def test_input2():
    hv = np.array([100, 200, 300, 400])
    tht_ph = np.array([10, 20, 30, 40])
    phi_ph = np.array([45, 60, 75])
    return hv, tht_ph, phi_ph

def test_get_p_hv_perp_slit_output_shape(test_input2):
    hv, tht_ph, phi_ph = test_input2
    output = get_p_hv_perp_slit(hv, tht_ph, phi_ph)
    expected_shape = (len(phi_ph), len(tht_ph))
    assert output.shape == expected_shape

def test_get_p_hv_perp_slit_output_values(test_input2):
    hv, tht_ph, phi_ph = test_input2
    output = get_p_hv_perp_slit(hv, tht_ph, phi_ph)
    expected_output = hv*(np.cos(np.radians(tht_ph[None, :])) * np.sin(np.radians(phi_ph[:, None]))) * 2 * np.pi / 12400
    assert np.allclose(output, expected_output)


