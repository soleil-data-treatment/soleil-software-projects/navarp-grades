from navarp.extras.srcnn import SrCNN
from navarp.utils.denoise import denoise_slice
import pytest
import numpy as np
import torch

@pytest.fixture(scope="module")
def model():
    return SrCNN()

def test_model_output_shape(model):
    input_shape = (1, 1, 128, 32)
    x = torch.randn(input_shape)
    output = model(x)
    expected_shape = (1, 1, 128, 32)
    assert output.shape == expected_shape

def test_many_model_output_shape(model):
    input_shape = (15, 1, 128, 32)
    x = torch.randn(input_shape)
    output = model(x)
    expected_shape = (15, 1, 128, 32)
    assert output.shape == expected_shape

def test_model_output_range(model):
    input_shape = (1, 1, 32, 32)
    x = torch.randn(input_shape)
    output = model(x)
    assert torch.all(output >= 0)
    assert torch.all(output <= 1)


def test_denoise_slice_shape():
    # Create a small 2D numpy array with known dimensions
    data = np.random.rand(20, 30).astype(np.float32)

    # Pass the data through the denoise_slice function
    denoised_spectra = denoise_slice(data)

    # Verify that the output has the expected shape
    assert denoised_spectra.shape == (20, 30)